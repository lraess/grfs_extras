# GRFS extras

## Gaussian Random Field Simulator, extra material

Copyright (C) 2018  Ludovic Raess, Dmitriy Kolyukhin and Alexander Minakov.

Extra codes related to the Computer and Geosciences publication upon request to the authors.

### GRFS was released in Computers & Geosciences:

```
@article{randomfield_rass_2019,
	title = "Efficient parallel random field generator for large 3-D geophysical problems",
	journal = "Computers & Geosciences",
 	volume = "131",
	pages = "158 - 169",
	year = "2019",
	issn = "0098-3004",
	doi = "https://doi.org/10.1016/j.cageo.2019.06.007",
	url = "http://www.sciencedirect.com/science/article/pii/S0098300418309944",
	author = "Ludovic Räss and Dmitriy Kolyukhin and Alexander Minakov",
	keywords = "Geophysics, Geostatistics, Seismology, Computational methods, Parallel and high-performance computing",
}
```

### Distributed software, extra directory content:

### GRFS_routines_fft

FFT-based routines:

* To benchmark the GRFS_exp routines
* Includes 2D and 3D CUDA FFT routines
* Includes 3D MATLAB FFT routine (benchmark)

### GRFS_routines_wave

3-D wave propagation in random media:

* Used for the coda waves analysis
* Includes 3D CUDA P-wave simulator
* Includes 3D MATLAB P-wave simulator (benchmark)

### Contact: ludovic.rass@gmail.com

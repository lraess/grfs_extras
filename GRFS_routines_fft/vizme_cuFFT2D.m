clear
% Deal with data produced by GPU_GRFS_FFT code
infos = load('infos.inf');  PRECIS=infos(1); nx=infos(2); ny=infos(3);
if (PRECIS==8), DAT = 'double';  elseif (PRECIS==4), DAT = 'single';  end 
fid = fopen('Nr.res','rb'); D = fread(fid, inf, DAT, 0, 'n');  fclose(fid);
D   = reshape(D,[nx,ny]);
% plot
figure(2),clf
imagesc(D'),axis xy,colorbar
drawnow


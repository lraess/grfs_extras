clear
% Parameters
Ix     = 80;%500;
Iy     = 40;%100;
Iz     = 100;
% grid extend
nx     = 100;%501;               % Must be odd
ny     = 80;%251;
nz     = 1;%251;
% inner points
nx_inn = 60;%301;           % Must be odd
ny_inn = 30;%151;
nz_inn = 1;%151;
% numerical step length
dx     = 10;
dy     = 10;
dz     = 10;
% std dev
sigma  = 2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Grid - Extendd domain
x      = 0:dx:dx*(nx-1);
y      = 0:dy:dy*(ny-1);
z      = 0:dz:dz*(nz-1);
[X,Y,Z] = ndgrid(x,y,z);
% Grid - Inner subdomain
dx_inn = 0.5*(nx-nx_inn);
dy_inn = 0.5*(ny-ny_inn);
dz_inn = 0.5*(nz-nz_inn);
x_inn  = 0:dx:dx*(nx_inn-1);
y_inn  = 0:dy:dy*(ny_inn-1);
z_inn  = 0:dz:dz*(nz_inn-1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Random field generation
% m=rand(nx,ny,nz) - 0.5;%array of random numbers
m      = randn(nx,ny,nz); %array of standard normal random numbers
fm     = fftn(m);
cx     = (nx-1)*dx/2;
cy     = (ny-1)*dy/2;
cz     = (nz-1)*dz/2;
Nr     = zeros(nx,ny,nz);
for ix=1:nx
    for iy=1:ny
        for iz=1:nz
             Nr(ix,iy,iz) = exp(-sqrt((x(ix)-cx)^2/Ix^2 + (y(iy)-cy)^2/Iy^2 + (z(iz)-cz)^2/Iz^2));
        end
    end
end 
fNr    = fftn(Nr) ;
fNr    = sqrt(fNr) ;
fm_flt = fm.*fNr ;
dv     = sigma*real(ifftn(fm_flt));
% dv = dv/max(dv(:));

v(:,:) = dv(:,:,1);
v_inn  = v([dx_inn+1:dx_inn+nx_inn],[dy_inn+1:dy_inn+ny_inn]);

figure(1),imagesc(v_inn'),colormap(flipud(jet)),colorbar,drawnow
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Covariances testing
Xtest  = 20;%50;
Ytest  = 20;%50;
C_true = zeros(nx_inn,ny_inn);    %  True covariance
for ix=1:nx_inn
    for iy=1:ny_inn
        C_true(ix,iy) = exp(-sqrt( (x_inn(ix)-(Xtest-1)*dx)^2/Ix^2 + (y_inn(iy)-(Ytest-1)*dy)^2/Iy^2 ));
    end
end
C_true = sigma^2*C_true;
C      = zeros(nx_inn,ny_inn);
NA     = 10000;
tic
for iNA=1:NA
    m      = randn(nx,ny,nz);%array of random numbers
    fm     = fftn(m);
    fm_flt = fm.*fNr;
    dv     = sigma*real(ifftn(fm_flt));
    v(:,:) = dv(:,:,1);
    v_inn  = v([dx_inn+1:dx_inn+nx_inn],[dy_inn+1:dy_inn+ny_inn]);
    for i=1:nx_inn
        for j=1:ny_inn
            C(i,j)= C(i,j) + v_inn(Xtest,Ytest)*v_inn(i,j);
        end
    end
end
toc
C   = C/NA;      % Estimated covariance
ER  = C-C_true;  % Error
figure(2),
subplot(311),imagesc(x,y,C_true'),axis image,colorbar
subplot(312),imagesc(x,y,C'),axis image,colorbar  
subplot(313),imagesc(x,y,ER'),axis image,colorbar
drawnow

// nvcc cufft_3D_v1.cu -arch=sm_70 -DNB=4 -DUSE_SINGLE_PRECISION -l cufft

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <cufft.h>
#include <cuComplex.h>

// #define USE_SINGLE_PRECISION    /* Comment this line using "!" if you want to use double precision.  */
#ifdef USE_SINGLE_PRECISION
#define DAT     float
#define Complex cuFloatComplex
#define PRECIS  4
#else
#define DAT     double
#define Complex cuDoubleComplex
#define PRECIS  8
#endif

#define GPU_ID 0

#define PI 3.14159265358979323846
#define init_real(A) DAT *A##_h, *A##_d; A##_h = (DAT*)malloc(sizeof(DAT)*NX*NY*NZ*BATCH); \
                     for(int i=0; i<(NX*NY*NZ*BATCH); i++) A##_h[i] = (DAT)0.0; \
                     cudaMalloc(&A##_d,      sizeof(DAT)*NX*NY*NZ*BATCH); \
                     cudaMemcpy( A##_d,A##_h,sizeof(DAT)*NX*NY*NZ*BATCH,cudaMemcpyHostToDevice);

#define init_RNDN(A) DAT *A##_h, *A##_d; A##_h = (DAT*)malloc(sizeof(DAT)*NX*NY*NZ*BATCH); \
                     for(int i=0; i<(NX*NY*NZ*BATCH); i++){ \
                       scaled   = ((double)rand() / (double)RAND_MAX); \
                       scaled2  = ((double)rand() / (double)RAND_MAX); \
                       scaled   = sqrt(-(double)2.0*log(scaled))*cos((double)2.0*PI*scaled2); \
                       A##_h[i] = (DAT)scaled; } \
                     cudaMalloc(&A##_d,      sizeof(DAT)*NX*NY*NZ*BATCH); \
                     cudaMemcpy( A##_d,A##_h,sizeof(DAT)*NX*NY*NZ*BATCH,cudaMemcpyHostToDevice);

#define alloc_complex(C) Complex *C##_h, *C##_d; C##_h = (Complex*)malloc(sizeof(Complex)*NX*NY*(NZ/2+1)*BATCH); \
                         cudaMalloc(&C##_d, sizeof(Complex)*NX*NY*(NZ/2+1)*BATCH);

#define gather_real(A)     cudaMemcpy(A##_h, A##_d, sizeof(DAT)*NX*NY*NZ*BATCH,          cudaMemcpyDeviceToHost);
#define gather_complex(C)  cudaMemcpy(C##_h, C##_d, sizeof(Complex)*NX*NY*(NZ/2+1)*BATCH,cudaMemcpyDeviceToHost);
#define free_all(A)        free(A##_h); cudaFree(A##_d);

#ifdef USE_SINGLE_PRECISION
#define cufftExecFWD(fwd_plan, A_d, C_d) cufftExecR2C(fwd_plan, A_d, C_d)
#define cufftExecINV(inv_plan, C_d, A_d) cufftExecC2R(inv_plan, C_d, A_d)
#define CUFFT_R_C CUFFT_R2C
#define CUFFT_C_R CUFFT_C2R
#else
#define cufftExecFWD(fwd_plan, A_d, C_d) cufftExecD2Z(fwd_plan, A_d, C_d)
#define cufftExecINV(inv_plan, C_d, A_d) cufftExecZ2D(inv_plan, C_d, A_d)
#define CUFFT_R_C CUFFT_D2Z
#define CUFFT_C_R CUFFT_Z2D
#endif

__host__ __device__ static __inline__ Complex cuCsqrt(Complex x){
  #ifdef USE_SINGLE_PRECISION
  double radius = cuCabsf(x);
  #else
  double radius = cuCabs(x);
  #endif
  double cosA = x.x / radius;
  Complex out;
  out.x = sqrt(radius * (cosA + 1.0) / 2.0);
  out.y = sqrt(radius * (1.0 - cosA) / 2.0);
  // signbit should be false if x.y is negative
  if (signbit(x.y)){ out.y *= -1.0; }
  return out;
}

////////// ========== Simulation Initialisation ========== //////////
#define BLOCK_X  16
#define BLOCK_Y  8
#define BLOCK_Z  8
#define GRID_X  (1*NB)
#define GRID_Y  (2*NB)
#define GRID_Z  (2*NB)
#define NX      (GRID_X*BLOCK_X)
#define NY      (GRID_Y*BLOCK_Y)
#define NZ      (GRID_Z*BLOCK_Z)
#define NDIMS    3

const DAT Lx  = 3000.0;
const DAT Ly  = Lx;
const DAT Lz  = Lx;
const DAT Ix  = 500.0;
const DAT Iy  = 200.0;
const DAT Iz  = 100.0;
const DAT sig = 2.0;
// Preprocessing ------------------------------------------------------------------------
const DAT dx  = Lx/(DAT)NX;
const DAT dy  = Ly/(DAT)NY;
const DAT dz  = Lz/(DAT)NZ;
const DAT sc  = 1./(NX*NY*NZ);
// FFT stuff
#define BATCH 1
int n[NDIMS]  = {NX, NY, NZ};
int idist     = NX*NY*NZ;
int odist     = NX*NY*(NZ/2+1);
int inembed[] = {NX, NY, NZ};
int onembed[] = {NX, NY, NZ/2+1};
int istride   = 1;
int ostride   = 1;

size_t workSize[1] = {0};
////////// ========== CUDA & tic() toc() subroutines ========== //////////
int gpuIdToSelect=-99, gpuId=-1;
dim3 grid, block;
void  init_cuda(){ 
  block.x = BLOCK_X; grid.x = GRID_X;
  block.y = BLOCK_Y; grid.y = GRID_Y;
  block.z = BLOCK_Z; grid.z = GRID_Z;
  cudaDeviceReset(); cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);  // set L1 to prefered
  gpuIdToSelect = GPU_ID; cudaSetDevice(gpuIdToSelect); cudaGetDevice(&gpuId); // choose GPU Id manually if not MPI
  printf("Launching GPU with Id=%d ...\n",gpuId);
}

void  clean_cuda(){ 
  cudaError_t ce = cudaGetLastError();
  if(ce != cudaSuccess){ printf("ERROR launching GPU C-CUDA program: %s\n", cudaGetErrorString(ce)); cudaDeviceReset();}
}

void save_info(){
  FILE* fid;
  fid=fopen("infos.inf","w"); fprintf(fid,"%d %d %d %d",PRECIS,NX,NY,NZ); fclose(fid);
}

void save_array(DAT* A, size_t nb_elems, const char A_name[]){
  char* fname; FILE* fid;
  asprintf(&fname, "%s.res" , A_name); 
  fid=fopen(fname, "wb"); fwrite(A, PRECIS, nb_elems, fid); fclose(fid); free(fname);
}
#define SaveArray(A,A_name)  gather_real(A); save_info(); save_array(A##_h, (NX*NY*NZ), A_name);
// Timer
double timer_start = 0;
double cpu_sec(){ struct timeval tp; gettimeofday(&tp,NULL); return tp.tv_sec+1e-6*tp.tv_usec; }
void   tic(){ timer_start = cpu_sec(); }
double toc(){ return cpu_sec()-timer_start; }
void   tim(const char *what, double n){ double s=toc(); printf("%s: %8.3f seconds",what,s);if(n>0)printf(", %8.3f GB/s", n/s); printf("\n"); }

////////// ========== Compue kernels ========== //////////
__global__ void compute1(DAT *Nr, const DAT Ix, const DAT Iy, const DAT Iz, const DAT dx, const DAT dy, const DAT dz, const int nx, const int ny, const int nz){  
    
  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

  DAT x = (DAT)ix*dx - (DAT)0.5*Lx;
  DAT y = (DAT)iy*dy - (DAT)0.5*Ly;
  DAT z = (DAT)iz*dz - (DAT)0.5*Lz;

  if (iz<nz && iy<ny && ix<nx){ Nr[ix + iy*nx + iz*nx*ny] = exp( -sqrt( (x*x)/(Ix*Ix) + (y*y)/(Iy*Iy) + (z*z)/(Iz*Iz) ) ); }
}

__global__ void compute2(Complex *fNr, const int nx, const int ny, const int nz){  
    
  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

  int nz2 = nz/2+1;

  if (iz<nz2 && iy<ny && ix<nx){ fNr[ix + iy*nx + iz*nx*ny] = cuCsqrt( fNr[ix + iy*nx + iz*nx*ny] ); }
}

__global__ void compute3(Complex *fNr, Complex *fM, const int nx, const int ny, const int nz){  
    
  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

  int nz2 = nz/2+1;

  #ifdef USE_SINGLE_PRECISION
  if (iz<nz2 && iy<ny && ix<nx){ fNr[ix + iy*nx + iz*nx*ny] = cuCmulf(fM[ix + iy*nx + iz*nx*ny] , fNr[ix + iy*nx + iz*nx*ny]); }
  #else
  if (iz<nz2 && iy<ny && ix<nx){ fNr[ix + iy*nx + iz*nx*ny] =  cuCmul(fM[ix + iy*nx + iz*nx*ny] , fNr[ix + iy*nx + iz*nx*ny]); }
  #endif
}

__global__ void compute4(DAT *Nr, const DAT sig, const DAT sc, const int nx, const int ny, const int nz){  
    
  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

  if (iz<nz && iy<ny && ix<nx){ Nr[ix + iy*nx + iz*nx*ny] = sc*sig*Nr[ix + iy*nx + iz*nx*ny]; }
}

int main(){
   init_cuda();
   printf("Local size: %dx%dx%d ...\n", NX, NY, NZ);
   printf("Launching (%dx%dx%d) grid of (%dx%dx%d) blocks.\n", grid.x, grid.y, grid.z, block.x, block.y, block.z);
   cufftHandle forward_plan1, forward_plan2, inverse_plan;
   double scaled=0.0, scaled2=0.0;
   srand(time(NULL));
   tic();
   // initialise arrays
   init_RNDN(M);
   init_real(Nr);
   // allocate GPU memory
   alloc_complex(fM);
   alloc_complex(fNr);
   // Prepare FFT plans
   cufftPlanMany(&forward_plan1, NDIMS, n, inembed, istride, idist, onembed, ostride, odist, CUFFT_R_C, BATCH);
   cufftPlanMany(&forward_plan2, NDIMS, n, inembed, istride, idist, onembed, ostride, odist, CUFFT_R_C, BATCH);
   cufftPlanMany(&inverse_plan , NDIMS, n, onembed, ostride, odist, inembed, istride, idist, CUFFT_C_R, BATCH);
   tim("GRFS 3D FFT-based [init]",0);
   // Get plan size
   cufftGetSizeMany(forward_plan2, NDIMS, n, inembed, istride, idist, onembed, ostride, odist, CUFFT_R_C, BATCH, workSize);
   printf("Plan size: %1.2e GB \n", (double)1.0e-9*(double)workSize[0]);
   // compute correlation length
   tic();
   compute1<<<grid,block>>>(Nr_d, Ix, Iy, Iz, dx, dy, dz, NX, NY, NZ); cudaDeviceSynchronize();
   // Perform FWD FFT 1
   cufftExecFWD(forward_plan1, M_d, fM_d); cudaDeviceSynchronize();
   // Perform FWD FFT 2
   cufftExecFWD(forward_plan2, Nr_d, fNr_d); cudaDeviceSynchronize();
   // compute sqrt
   compute2<<<grid,block>>>(fNr_d, NX, NY, NZ); cudaDeviceSynchronize();
   // compute multiply
   compute3<<<grid,block>>>(fNr_d, fM_d, NX, NY, NZ); cudaDeviceSynchronize();
   // Perform INV FFT
   cufftExecINV(inverse_plan, fNr_d, Nr_d); cudaDeviceSynchronize();
   // compute scale
   compute4<<<grid,block>>>(Nr_d, sig, sc, NX, NY, NZ); cudaDeviceSynchronize();
   tim("GRFS 3D FFT-based [compute]",0);

   SaveArray(Nr , "Nr_3D")

   cufftDestroy(forward_plan1);
   cufftDestroy(forward_plan2);
   cufftDestroy(inverse_plan);

   free_all(M);
   free_all(Nr);
   free_all(fM);
   free_all(fNr);

   clean_cuda();
}

clear % Wave 2D vectorised
% Physics
Lx   = 3000;
Ly   = 1600;
Lz   = 2000;
Labs = 400;
Vs   = 3200;
Vi   = 1.2;
radi = 200;
% Numerics
nx   = 151;
ny   = 81;
nz   = 101;
nt   = 1500;
nout = 50;
gam  = 0.04; % absorbing boundaries
% Preprocessing
dx   = Lx/nx;
dy   = Ly/ny;
dz   = Lz/nz;
x    = dx/2:dx:Lx-dx/2;
y    = dy/2:dy:Ly-dy/2;
z    = dz/2:dz:Lz-dz/2;
[X3,Y3,Z3] = ndgrid(x,y,z);
% Generate spherical perturbation
v    = Vs*ones(nx,ny,nz);
% rad  = (X3-xc(1)).^2+(Y3-xc(2)).^2+(Z3-xc(3)).^2;
% v(rad<radi^2) = v(rad<radi^2).*Vi;
dv   = vmodel(500,500,500,X3,Y3,Z3,nx,ny,nz,30);
dv(:,:,1:floor(Labs/dz)) = 0;
v    = v + dv;
%Acquisition geometry
% x_s  = [Labs+1,max(y)/2,Labs+1];
x_s  = [max(x)/2,max(y)/2,Lz-Labs];
% x_s2 = floor((Lz-Labs)/dz);
x_r  = [];
x_r(:,1) = Labs+200+dx/2:25:2.5e3-dx/2;
x_r(:,2) = max(y)/2;
x_r(:,3) = Labs;
% Source location
xs   = round((x_s(:,1)-min(x))/dx);
ys   = round((x_s(:,2)-min(y))/dy);
zs   = round((x_s(:,3)-min(z))/dz);
% Receiver location
xr   = round((x_r(:,1)-min(x))/dx);
yr   = round((x_r(:,2)-min(y))/dy);
zr   = round((x_r(:,3)-min(z))/dz);
%seismogram array
u    = zeros(nt,length(xr));
% Initial conditions
% P    = exp(-(X3-Lx/2).^2/lam -(Y3-Ly/2).^2/lam -(Z3-Lz/2).^2/lam);
P    = zeros(nx,ny,nz);
d2Px = zeros(nx,ny,nz);
d2Py = zeros(nx,ny,nz);
d2Pz = zeros(nx,ny,nz);
dPx  = zeros(nx,ny,nz);
dPy  = zeros(nx,ny,nz);
dPz  = zeros(nx,ny,nz);
dt   = 0.1*min([dx,dy,dz])/max(abs(v(:))); % min(dx,dy)/sqrt(k*k/rho)/2.1; rho=cst, k=k^2
Fs   = 1/dt;         % sampling frequency (Hz)
t    = (0:nt-1)*dt;  % time array
f0   = 10;           % peak freq
t0   = 1/f0;
src  = exp(-1/2*(4*f0)^2*(t-t0).^2);
st   = 1e3*src/max(src);
% figure(2),plot(t,st),drawnow
% Visualise
X3p = permute(X3,[2,1,3]); Y3p = permute(Y3,[2,1,3]); Z3p = permute(Z3,[2,1,3]);
figure(3),clf,slice(X3p,Y3p,Z3p,permute(v,[2,1,3]),Lx/2,Ly/2,Lz/2), xlabel('x'),ylabel('y'),zlabel('z')
set(gca,'ZDir','reverse'),shading flat,colorbar('location','south'),colormap(flipud(jet))
hold on, plot3(x(xs),y(ys),z(zs),'*r'), plot3(x(xr),y(yr),z(zr),'^b')
axis equal,axis tight,drawnow
% Action
Po=P; Pn=P; tim=0;
for it = 1:nt-2
    % 4th order in space
    d2Px(3:end-2,3:end-2,3:end-2) = 1/12*(-P(5:end,3:end-2,3:end-2)+16*P(4:end-1,3:end-2,3:end-2)-30*P(3:end-2,3:end-2,3:end-2)+16*P(2:end-3,3:end-2,3:end-2)-P(1:end-4,3:end-2,3:end-2))/dx/dx;
    d2Py(3:end-2,3:end-2,3:end-2) = 1/12*(-P(3:end-2,5:end,3:end-2)+16*P(3:end-2,4:end-1,3:end-2)-30*P(3:end-2,3:end-2,3:end-2)+16*P(3:end-2,2:end-3,3:end-2)-P(3:end-2,1:end-4,3:end-2))/dy/dy;
    d2Pz(3:end-2,3:end-2,3:end-2) = 1/12*(-P(3:end-2,3:end-2,5:end)+16*P(3:end-2,3:end-2,4:end-1)-30*P(3:end-2,3:end-2,3:end-2)+16*P(3:end-2,3:end-2,2:end-3)-P(3:end-2,3:end-2,1:end-4))/dz/dz;
    % 2nd order in time
    Pn = 2*P - Po + dt.*dt.*v.*v.*(d2Px + d2Py + d2Pz);
    Pn(xs,ys,zs) = Pn(xs,ys,zs) + st(it)*dt^2;    % add source term
%     Pn(3:end-2,3:end-2,x_s2) = Pn(3:end-2,3:end-2,x_s2) + st(it)*dt^2;    % add source term
    % BC
    dPx(3:end-2,3:end-2,3:end-2) = (P(4:end-1,3:end-2,3:end-2) - P(3:end-2,3:end-2,3:end-2))/dx;
    dPy(3:end-2,3:end-2,3:end-2) = (P(3:end-2,4:end-1,3:end-2) - P(3:end-2,3:end-2,3:end-2))/dy;
    dPz(3:end-2,3:end-2,3:end-2) = (P(3:end-2,3:end-2,4:end-1) - P(3:end-2,3:end-2,3:end-2))/dz;
    xLabs1 = fix(     Labs/dx); yLabs1 = fix(     Labs/dy); zLabs1 = fix(     Labs/dz);
    xLabsE = fix((Lx-Labs)/dx); yLabsE = fix((Ly-Labs)/dy); zLabsE = fix((Lz-Labs)/dz);
    % xmin
    Pn(1:xLabs1,:,:) = Pn(1:xLabs1,:,:) + gam*((X3(1:xLabs1,:,:)-Labs)/Labs).*((Pn(1:xLabs1,:,:)-P(1:xLabs1,:,:)) + v(1:xLabs1,:,:).*dPx(1:xLabs1,:,:)*dt);
    Pn(:,1:yLabs1,:) = Pn(:,1:yLabs1,:) + gam*((Y3(:,1:yLabs1,:)-Labs)/Labs).*((Pn(:,1:yLabs1,:)-P(:,1:yLabs1,:)) + v(:,1:yLabs1,:).*dPy(:,1:yLabs1,:)*dt);
    Pn(:,:,1:zLabs1) = Pn(:,:,1:zLabs1) + gam*((Z3(:,:,1:zLabs1)-Labs)/Labs).*((Pn(:,:,1:zLabs1)-P(:,:,1:zLabs1)) + v(:,:,1:zLabs1).*dPz(:,:,1:zLabs1)*dt);
    % xmax
    Pn(xLabsE:end,:,:) = Pn(xLabsE:end,:,:) + gam*((Lx-X3(xLabsE:end,:,:)-Labs)/Labs).*((Pn(xLabsE:end,:,:)-P(xLabsE:end,:,:)) - v(xLabsE:end,:,:).*dPx(xLabsE:end,:,:)*dt);
    Pn(:,yLabsE:end,:) = Pn(:,yLabsE:end,:) + gam*((Ly-Y3(:,yLabsE:end,:)-Labs)/Labs).*((Pn(:,yLabsE:end,:)-P(:,yLabsE:end,:)) - v(:,yLabsE:end,:).*dPy(:,yLabsE:end,:)*dt);
    Pn(:,:,zLabsE:end) = Pn(:,:,zLabsE:end) + gam*((Lz-Z3(:,:,zLabsE:end)-Labs)/Labs).*((Pn(:,:,zLabsE:end)-P(:,:,zLabsE:end)) - v(:,:,zLabsE:end).*dPz(:,:,zLabsE:end)*dt);
    % Updates
    Po=P; P=Pn;
    u(it,:) = P(xr,yr(1),zr(1)); % seismogram output
    tim = tim + dt;
    % Plot
    if rem(it,nout)==0
        disp(['Time step: ', num2str(it), ' Time:  ', num2str(tim)]);
        figure(11),clf
        slice(X3p,Y3p,Z3p,permute(P,[2,1,3]),Lx/2,Ly/2,Lz/2), shading interp,
        grid off, set(gca,'Zdir','reverse'), axis xy, axis equal, hold on
        plot3(x(xs),y(ys),z(zs),'*r'),plot3(x(xr),y(yr),z(zr),'^b'), axis tight
        colormap(gray),xlabel('x'),ylabel('y'),title(['maxA ',num2str(max(P(:)))])
        view(0,0),zlabel('z')
        drawnow
%         figure(3),clf,imagesc(squeeze(P(:,ys,:))'),colorbar
%         drawnow
%         itcount = num2str(it,'%5.5d');
%         outfile = ['Pwave3D_',itcount];
%         save(outfile,'P')
    end
end
figure(4),imagesc(x_r(:,1),t,u),xlabel('x dist.'),ylabel('time')

%% supporting function random field
function dv = vmodel(a,b,c,X3,Y3,Z3,nx,ny,nz,scale)
m      = rand(nx,ny,nz) - 0.5; % array of random numbers
fm     = fftn(m);
Nr     = exp(-sqrt(X3.^2/a^2 + Y3.^2/b^2 + Z3.^2/c^2));
fm_flt = fm.*fftn(Nr) ;
dv     = real(ifftn(fm_flt));
dv     = scale*dv/max(dv(:));
end

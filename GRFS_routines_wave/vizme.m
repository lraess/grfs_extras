clear
machineformat='n';  skip=0; OVERLAP = 2;
nxyz   = load('0_nxyz.inf'  );  PRECIS=nxyz(1); nx=nxyz(2); ny=nxyz(3); nz=nxyz(4);
dims   = load('0_dims.inf'  );  nprocs=dims(1); dims=dims(2:4);
nxyz_l = load('0_nxyz_l.inf');
infos  = load('0_info.inf'  );  nt=infos(1); nsave=infos(2);
coords = zeros(nprocs,3);
for  ip = 1:nprocs
    coords(ip,:) = load([ num2str(ip-1) '_co.inf']);
end
if (PRECIS==8), DAT = 'double';  elseif (PRECIS==4), DAT = 'single';  end

nsteps = nt/nsave;
for isave = 0:(nsteps-1)
    figure(1),clf
    P = load_field('P', nxyz_l, nxyz_l, OVERLAP, nprocs, coords, DAT, isave, skip, machineformat);
    % if isnan(P(1)),break,end
    P_flat = squeeze(P(:,fix(ny/2),:));
    subplot(221),imagesc(flipud(P_flat')), colorbar,axis image
    title('P pert.')
    caxis([0 1]*2e-1)

    Pb = load_field('Pb', nxyz_l, nxyz_l, OVERLAP, nprocs, coords, DAT, isave, skip, machineformat);
    % if isnan(P(1)),break,end
    Pb_flat = squeeze(Pb(:,fix(ny/2),:));
    subplot(222),imagesc(flipud(Pb_flat')), colorbar,axis image
    title('P base')
    caxis([0 1]*2e-1)
    
    X = (P-Pb);
    X_flat = squeeze(X(:,fix(ny/2),:));
    subplot(223),imagesc(flipud(X_flat')), colorbar,axis image
    title('P pert. - P base')
    caxis([-1 1]*3e-2)
    
    isave2=0;
    nnn = [nt 1 1];
    Src = load_field('Src', nnn, nnn, OVERLAP, nprocs, coords, DAT, isave2, skip, machineformat);
    subplot(224),plot(1:nt,Src,'-',isave*nsave+1,Src(isave*nsave+1),'ro','Linewidth',1.5),axis([0 nt -10 1010])
    drawnow
    print(['FDTD_rnd_N1_' int2str(isave)],'-dpng','-r300')
    
    if isave==0
    figure(2),clf
    DV = load_field('DV', nxyz_l, nxyz_l, OVERLAP, nprocs, coords, DAT, isave2, skip, machineformat);
    % if isnan(P(1)),break,end
    DV_flat = squeeze(DV(:,:,fix(nz/2)));
    imagesc(DV_flat'),axis xy,colorbar,axis image, title(max(abs(P(:))))
    title('RND vel. field')
    drawnow
    print('FDTD_rndField_N1','-dpng','-r300')
    end
    
%     save(['Mat_out1_' int2str(isave)],'P','Pb','DV','Src','nt','nsave','nsteps')
end

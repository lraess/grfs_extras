// include file for non MPI 3D geocomputing, 05.10.2018, Ludovic Raess

#define zeros(A,nx,ny,nz)  DAT *A##_d,*A##_h; A##_h = (DAT*)malloc(((nx)*(ny)*(nz))*sizeof(DAT)); \
                           for(i=0; i < ((nx)*(ny)*(nz)); i++){ A##_h[i]=(DAT)0.0; }              \
                           cudaMalloc(&A##_d      ,((nx)*(ny)*(nz))*sizeof(DAT));                 \
                           cudaMemcpy( A##_d,A##_h,((nx)*(ny)*(nz))*sizeof(DAT),cudaMemcpyHostToDevice);
#define free_all(A)        free(A##_h); cudaFree(A##_d);
#define gather(A,nx,ny,nz) cudaMemcpy( A##_h,A##_d,((nx)*(ny)*(nz))*sizeof(DAT),cudaMemcpyDeviceToHost);
#define   push(A,nx,ny,nz) cudaMemcpy( A##_d,A##_h,((nx)*(ny)*(nz))*sizeof(DAT),cudaMemcpyHostToDevice);
#define d2Adx2(A,fdx)      ( fdx*( -           A[(ix+4) + (iy+2)*nx + (iz+2)*nx*ny] \
                                   + (DAT)16.0*A[(ix+3) + (iy+2)*nx + (iz+2)*nx*ny] \
                                   - (DAT)30.0*A[(ix+2) + (iy+2)*nx + (iz+2)*nx*ny] \
                                   + (DAT)16.0*A[(ix+1) + (iy+2)*nx + (iz+2)*nx*ny] \
                                   -           A[(ix  ) + (iy+2)*nx + (iz+2)*nx*ny] ) )
#define d2Ady2(A,fdy)      ( fdy*( -           A[(ix+2) + (iy+4)*nx + (iz+2)*nx*ny] \
                                   + (DAT)16.0*A[(ix+2) + (iy+3)*nx + (iz+2)*nx*ny] \
                                   - (DAT)30.0*A[(ix+2) + (iy+2)*nx + (iz+2)*nx*ny] \
                                   + (DAT)16.0*A[(ix+2) + (iy+1)*nx + (iz+2)*nx*ny] \
                                   -           A[(ix+2) + (iy  )*nx + (iz+2)*nx*ny] ) )
#define d2Adz2(A,fdz)      ( fdz*( -           A[(ix+2) + (iy+2)*nx + (iz+4)*nx*ny] \
                                   + (DAT)16.0*A[(ix+2) + (iy+2)*nx + (iz+3)*nx*ny] \
                                   - (DAT)30.0*A[(ix+2) + (iy+2)*nx + (iz+2)*nx*ny] \
                                   + (DAT)16.0*A[(ix+2) + (iy+2)*nx + (iz+1)*nx*ny] \
                                   -           A[(ix+2) + (iy+2)*nx + (iz  )*nx*ny] ) )
#define dAdx(A)            ( A[(ix+3) + (iy+2)*nx + (iz+2)*nx*ny] - A[(ix+2) + (iy+2)*nx + (iz+2)*nx*ny] )
#define dAdy(A)            ( A[(ix+2) + (iy+3)*nx + (iz+2)*nx*ny] - A[(ix+2) + (iy+2)*nx + (iz+2)*nx*ny] )
#define dAdz(A)            ( A[(ix+2) + (iy+2)*nx + (iz+3)*nx*ny] - A[(ix+2) + (iy+2)*nx + (iz+2)*nx*ny] )
#define all(A)             ( A[(ix  ) + (iy  )*nx + (iz  )*nx*ny] )
#define inn2(A)            ( A[(ix+2) + (iy+2)*nx + (iz+2)*nx*ny] )
#define swap(A,B)          DAT *swap; swap = A##_d; A##_d = B##_d; B##_d = swap;

int dims[3]={1,1,1};
int coords[3]={0,0,0};
int* coords_d=NULL;
int nprocs=1, me_loc=1, me=0, gpu_id=-1;

// Timer
#include "sys/time.h"
double timer_start = 0;
double cpu_sec(){ struct timeval tp; gettimeofday(&tp,NULL); return tp.tv_sec+1e-6*tp.tv_usec; }
void   tic(){ timer_start = cpu_sec(); }
double toc(){ return cpu_sec()-timer_start; }
void   tim(const char *what, double n){ double s=toc();if(me==0){ printf("%s: %8.3f seconds",what,s);if(n>0)printf(", %8.3f GB/s", n/s); printf("\n"); } }

void  clean_cuda(){ 
    cudaError_t ce = cudaGetLastError();
    if(ce != cudaSuccess){ printf("ERROR launching GPU C-CUDA program: %s\n", cudaGetErrorString(ce)); cudaDeviceReset(); }
}

#define set_up_grid()  dim3 grid, block; \
    block.x = BLOCK_X; grid.x = GRID_X; \
    block.y = BLOCK_Y; grid.y = GRID_Y; \
    block.z = BLOCK_Z; grid.z = GRID_Z;

#define set_up_parallelisation()  gpu_id=GPU_ID; cudaSetDevice(gpu_id); cudaGetDevice(&gpu_id); cudaDeviceReset(); cudaDeviceSetCacheConfig(cudaFuncCachePreferL1); \
                                  cudaMalloc(&coords_d,3*sizeof(int)); cudaMemcpy(coords_d ,coords,3*sizeof(int),cudaMemcpyHostToDevice);
#define update_sides(A,nx_A,ny_A,nz_A)
#define init_sides(A,nx_A,ny_A,nz_A)
#define free_sides(A)
#define MPI_Finalize()


DAT device_MAX=0.0;
#define NB_THREADS     (BLOCK_X*BLOCK_Y*BLOCK_Z)
#define blockId        (blockIdx.x  +  blockIdx.y *gridDim.x +  blockIdx.z *gridDim.x *gridDim.y)
#define threadId       (threadIdx.x + threadIdx.y*blockDim.x + threadIdx.z*blockDim.x*blockDim.y)
#define isBlockMaster  (threadIdx.x==0 && threadIdx.y==0 && threadIdx.z==0)
// maxval //
#define block_max_init()  DAT __thread_maxval=0.0;
#define __thread_max(A,nx_A,ny_A,nz_A)  if (iz<nz_A && iy<ny_A && ix<nx_A){ __thread_maxval = max((__thread_maxval) , (A[ix + iy*nx_A + iz*nx_A*ny_A])); } 

__shared__ volatile  DAT __block_maxval;
#define __block_max(A,nx_A,ny_A,nz_A)  __thread_max(A,nx_A,ny_A,nz_A);  if (isBlockMaster){ __block_maxval=0; }  __syncthreads(); \
                                       for (int i=0; i < (NB_THREADS); i++){ if (i==threadId){ __block_maxval = max(__block_maxval,__thread_maxval); }  __syncthreads(); }

__global__ void __device_max_d(DAT*A, const int nx_A,const int ny_A,const int nz_A, DAT*__device_maxval){
  block_max_init();
  int ix  = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
  int iy  = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
  int iz  = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z
  // find the maxval for each block
  __block_max(A,nx_A,ny_A,nz_A);
  __device_maxval[blockId] = __block_maxval;
}

#define __MPI_max(A,nx_A,ny_A,nz_A)   __device_max_d<<<grid, block>>>(A##_d, nx_A,ny_A,nz_A, __device_maxval_d); \
                                      gather(__device_maxval,grid.x,grid.y,grid.z); device_MAX=(DAT)0.0;         \
                                      for (int i=0; i < (grid.x*grid.y*grid.z); i++){                            \
                                         device_MAX = max(device_MAX,__device_maxval_h[i]);                      \
                                      }                                                                          \
                                      A##_MAX = (device_MAX);

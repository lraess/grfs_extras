#!/bin/bash

# runme.sh is a shell script for executing GPU MPI application
# -> to make it executable: chmod +x runme.sh or chmod 755 runme.sh
#------------------------------------------------------------------

# load the reqired modules:
module load cuda/9.0

# remove what should be removed
rm a.out

# compile the code for Titan X
randF="$1"

if [ $# -ne 1 ]; then
    nvcc -arch=sm_70 --compiler-options -O3 GPU_Wave_3D_v5.cu
else
	nvcc -arch=sm_70 --compiler-options -O3 GPU_Wave_3D_v5.cu -D"${randF}"
fi

# execute it
./a.out

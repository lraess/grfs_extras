// Wave 3D GPU Cuda aware MPI
// nvcc -arch=sm_52 --compiler-options -O3 GPU_wave_3D_v5.cu -DRAND
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "cuda.h"
#define NDIMS  3

#define GPU_ID   0
#define USE_SINGLE_PRECISION    /* Comment this line using "//" if you want to use double precision.  */

#ifdef USE_SINGLE_PRECISION
#define DAT      float
#define PRECIS   4
#else
#define DAT      double
#define PRECIS   8
#endif

#define OVERLENGTH_X  0
#define OVERLENGTH_Y  0
#define OVERLENGTH_Z  0
// Physics
const DAT Lx    = 3000.0;
const DAT Ly    = 3000.0;
const DAT Lz    = 3000.0;
const DAT Vs    = 3200.0;
const DAT Labs  = 400.0;
// Random field gen. physics
const DAT scale = 1e3;
const DAT sf    = 1.0;                     // standard deviation
const DAT If[3] = {100.0, 100.0, 100.0};   // correlation length x,y,z
const int Ng    = 100000;                  // inner parameter, number of harmonics (by default Ng=100)
// Source
const DAT lam2  = 500.0; // std dev initial pressure
const DAT A0    = 1e3;   // amplitude of the perturbation
const DAT f0    = 15;    // peak freq.
const int ds    = 2;
#define x_s    (Lx - Lx/2.0)
#define y_s    (Ly - Ly/2.0)
// #define z_s    (Lz - Lz/2.0)
#define z_s    (Labs+1)
// Numerics
#define BLOCK_X  32
#define BLOCK_Y  16
#define BLOCK_Z  2
#define GRID_X   8*2
#define GRID_Y   8*4
#define GRID_Z   8*32
#define DIMS_X   D_x
#define DIMS_Y   D_y
#define DIMS_Z   D_z
#define nBC      10
const int nx    = BLOCK_X*GRID_X - OVERLENGTH_X;
const int ny    = BLOCK_Y*GRID_Y - OVERLENGTH_Y;
const int nz    = BLOCK_Z*GRID_Z - OVERLENGTH_Z;
const int nsave = 100;
const int nt    = nsave*80;
const DAT gam   = 0.04*5;
// Preprocessing
DAT    dx, dy, dz;
size_t Nix, Niy, Niz;
// MPI /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "geocomp_unil_NOmpi_grf_3D.h"
#include "GPU_grfa_v1.h"
// Suboutines /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void save_info(){
    FILE* fid;
    if (me==0){ fid=fopen("0_nxyz.inf"  ,"w"); fprintf(fid,"%d %d %d %d", PRECIS, Nix, Niy, Niz);             fclose(fid); }
    if (me==0){ fid=fopen("0_nxyz_l.inf","w"); fprintf(fid,"%d %d %d"   , nx, ny, nz);                        fclose(fid); }
    if (me==0){ fid=fopen("0_dims.inf"  ,"w"); fprintf(fid,"%d %d %d %d", nprocs, dims[0], dims[1], dims[2]); fclose(fid); }
    if (me==0){ fid=fopen("0_info.inf"  ,"w"); fprintf(fid,"%d %d"      , nt, nsave);                         fclose(fid); }
}
void save_coords(){
    char* fname; FILE* fid; asprintf(&fname, "%d_co.inf", me); 
    fid=fopen(fname, "w"); fprintf(fid,"%d %d %d",coords[0],coords[1],coords[2]); fclose(fid); free(fname);
}
void save_array(DAT* A, int nx, int ny, int nz, const char A_name[], int isave){
    char* fname; FILE* fid; asprintf(&fname, "%d_%d_%s.res" , isave, me, A_name);
    fid=fopen(fname, "wb"); fwrite(A, sizeof(DAT), (nx)*(ny)*(nz), fid); fclose(fid); free(fname);
}
#define SaveArray(A,nx,ny,nz,A_name) gather(A,nx,ny,nz); save_array(A##_h,nx,ny,nz,A_name, isave);

// Computing physics kernels /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void pert(const int nt, const DAT f0, const DAT A0, DAT dt, DAT* Src){
    int itr;
    DAT t0 = (DAT)1.0/f0;
    for (itr=0; itr<nt; itr++){
        DAT t    = ((DAT)itr)*dt;
        Src[itr] = A0*exp(-(DAT)0.5*((DAT)4.0*f0)*((DAT)4.0*f0)*(t-t0)*(t-t0));
    }
}

__global__ void init(const DAT A0, const DAT lam2, DAT* P, DAT* P2, DAT* DV, const DAT Vs, int* coords, const DAT Lx, const DAT Ly, const DAT Lz, DAT dx, DAT dy, DAT dz, const int nx, const int ny, const int nz){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

    #define Xc ((DAT)(coords[0]*(nx-2) + ix)*dx + dx/2 - (DAT)0.5*Lx)
    #define Yc ((DAT)(coords[1]*(ny-2) + iy)*dy + dy/2 - (DAT)0.5*Ly)
    #define Zc ((DAT)(coords[2]*(nz-2) + iz)*dz + dz/2 - (DAT)0.5*Lz)
    // if (iz<nz && iy<ny && ix<nx){  all(P)  = A0*exp(-(Xc*Xc)/lam2 -(Yc*Yc)/lam2 -(Zc*Zc)/lam2); }
    // if (iz<nz && iy<ny && ix<nx){  all(P2) = all(P); }
    if (iz<nz && iy<ny && ix<nx){  all(DV) = (all(DV) + Vs); }
    #undef Xc
    #undef Yc
    #undef Zc
}

__global__ void compute_P(DAT* P, DAT* P2, DAT* DV, DAT dt, DAT fdx,DAT fdy,DAT fdz, DAT dx, DAT dy, DAT dz, const int nx, const int ny, const int nz){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

    if (iz<(nz-4) && iy<(ny-4) && ix<(nx-4)){ inn2(P2) = (DAT)2.0*inn2(P) - inn2(P2) + dt*dt*inn2(DV)*inn2(DV)*(d2Adx2(P,fdx) + d2Ady2(P,fdy) + d2Adz2(P,fdz)); }
}

__global__ void source_P(DAT* P2, DAT* Src, int it, int xsrc, int ysrc, int zsrc, int ds, int xLabs1, int yLabs1, int zLabs1, int xLabsE, int yLabsE, int zLabsE, DAT dt, const int nx, const int ny, const int nz){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

    // if (iz<(zsrc+ds) && iz>(zsrc-ds) && iy<(ysrc+ds) && iy>(ysrc-ds) && ix<(xsrc+ds) && ix>(xsrc-ds)){ all(P2) = all(P2) + Src[it]*dt*dt; } //add source term
    if (iz<(zsrc+ds) && iz>(zsrc-ds) && iy<(yLabsE-5) && iy>(yLabs1+5) && ix<(xLabsE-5) && ix>(xLabs1+5)){ all(P2) = all(P2) + Src[it]*dt*dt; } //add source term
}
// Absorbing boundaries /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__global__ void absorbP_1(DAT* P, DAT* dPx, DAT* dPy, DAT* dPz, DAT* DV, DAT dt, DAT dx, DAT dy, DAT dz, const int nx, const int ny, const int nz){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

    if (iz<(nz-4) && iy<(ny-4) && ix<(nx-4)){ inn2(dPx) = (dt/dx)*inn2(DV)*dAdx(P); }
    if (iz<(nz-4) && iy<(ny-4) && ix<(nx-4)){ inn2(dPy) = (dt/dy)*inn2(DV)*dAdy(P); }
    if (iz<(nz-4) && iy<(ny-4) && ix<(nx-4)){ inn2(dPz) = (dt/dz)*inn2(DV)*dAdz(P); }
}

__global__ void absorbP_2(DAT* P, DAT* P2, DAT* dPx, DAT* dPy, DAT* dPz, int* coords, const DAT gam, const DAT Labs_1, int xLabs1, int yLabs1, int zLabs1, int xLabsE, int yLabsE, int zLabsE, const DAT Lx, const DAT Ly, const DAT Lz, DAT dx, DAT dy, DAT dz, const int nx, const int ny, const int nz){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

    #define Xc ((DAT)(coords[0]*(nx-2) + ix)*dx + dx/2 )
    #define Yc ((DAT)(coords[1]*(ny-2) + iy)*dy + dy/2 )
    #define Zc ((DAT)(coords[2]*(nz-2) + iz)*dz + dz/2 )
    if (iz<nz && iy<ny && ix<xLabs1){ all(P2) = all(P2) + gam*( (Labs_1*Xc -1.0) )*( (all(P2)-all(P)) + all(dPx) ); }
    if (iz<nz && iy<yLabs1 && ix<nx){ all(P2) = all(P2) + gam*( (Labs_1*Yc -1.0) )*( (all(P2)-all(P)) + all(dPy) ); }
    if (iz<zLabs1 && iy<ny && ix<nx){ all(P2) = all(P2) + gam*( (Labs_1*Zc -1.0) )*( (all(P2)-all(P)) + all(dPz) ); }

    if (iz<nz && iy<ny && ix<nx && ix>xLabsE){ all(P2) = all(P2) + gam*( Labs_1*(Lx - Xc) -1.0 )*( (all(P2)-all(P)) - all(dPx) ); }
    if (iz<nz && iy<ny && ix<nx && iy>yLabsE){ all(P2) = all(P2) + gam*( Labs_1*(Ly - Yc) -1.0 )*( (all(P2)-all(P)) - all(dPy) ); }
    if (iz<nz && iy<ny && ix<nx && iz>zLabsE){ all(P2) = all(P2) + gam*( Labs_1*(Lz - Zc) -1.0 )*( (all(P2)-all(P)) - all(dPz) ); }
    #undef Xc
    #undef Yc
    #undef Zc
}

int main(int argc, char *argv[]){
    int i, it;
    set_up_grid();
    set_up_parallelisation();
    if (me==0){ printf("Local sizes: Nx=%d, Ny=%d, Nz=%d. %d iterations. \n", nx,ny,nz,nt); }
    // Initial arrays
    zeros(P   ,nx,ny,nz);
    zeros(P2  ,nx,ny,nz);
    zeros(dPx ,nx,ny,nz);
    zeros(dPy ,nx,ny,nz);
    zeros(dPz ,nx,ny,nz);
    // Random Field
    zeros(DV ,nx,ny,nz);
    zeros(__device_maxval ,grid.x,grid.y,grid.z);
    DAT DV_MAX=0.0;
    zeros(Src ,nt,1,1);
    // MPI sides    
    // Preprocessing
    Nix  = ((nx-2)*dims[0])+2;
    Niy  = ((ny-2)*dims[1])+2;
    Niz  = ((nz-2)*dims[2])+2;
    dx   = Lx/(DAT)Nix;  // Global dx, dy
    dy   = Ly/(DAT)Niy;
    dz   = Lz/(DAT)Niz;
    // Generate random field
    #ifdef RAND
    generate_random_field();
    __MPI_max(DV ,nx,ny,nz);
    DAT dt = 0.1*min(min(dx,dy),dz)/(Vs+DV_MAX);
    #else
    // DAT dt = 2.7902e-04;
    DAT dt = 1.3951e-04;
    #endif
    printf("FDTD timestep: dt = %1.4e.\n",dt);
    // precompute derivative factors
    DAT fdx = (DAT)(1.0/12.0/dx/dx);
    DAT fdy = (DAT)(1.0/12.0/dy/dy);
    DAT fdz = (DAT)(1.0/12.0/dz/dz);
    DAT Labs_1 = 1.0/400.0;
    int xLabs1 = (int)round(Labs/dx-1),  xLabsE = (int)round((Lx-Labs)/dx);
    int yLabs1 = (int)round(Labs/dy-1),  yLabsE = (int)round((Ly-Labs)/dy);
    int zLabs1 = (int)round(Labs/dz-1),  zLabsE = (int)round((Lz-Labs)/dz);
    int xsrc = (int)round(x_s/dx);
    int ysrc = (int)round(y_s/dy);
    int zsrc = (int)round(z_s/dz);
    // Initial conditions
    int isave = 0;
    save_info(); save_coords();
    pert(nt, f0, A0, dt, Src_h); push(Src ,nt,1,1);
    init<<<grid,block>>>(A0, lam2, P_d, P2_d, DV_d, Vs, coords_d, Lx, Ly, Lz, dx, dy, dz, nx, ny, nz); cudaDeviceSynchronize();
    // Action
    for (it=0;it<nt;it++){
        if (it==3){ tic(); }
        compute_P<<<grid,block>>>(P_d, P2_d, DV_d, dt, fdx, fdy, fdz, dx, dy, dz, nx, ny, nz); cudaDeviceSynchronize();
        source_P<<<grid,block>>>(P2_d, Src_d, it, xsrc, ysrc, zsrc, ds, xLabs1, yLabs1, zLabs1, xLabsE, yLabsE, zLabsE, dt, nx, ny, nz); cudaDeviceSynchronize();
        if ((it%nBC)==0){ // to keep somehow the perf ...
            absorbP_1<<<grid,block>>>(P_d, dPx_d, dPy_d, dPz_d, DV_d, dt, dx, dy, dz, nx, ny, nz); cudaDeviceSynchronize();
            absorbP_2<<<grid,block>>>(P_d, P2_d, dPx_d, dPy_d, dPz_d, coords_d, gam, Labs_1, xLabs1, yLabs1, zLabs1, xLabsE, yLabsE, zLabsE, Lx, Ly, Lz, dx, dy, dz, nx, ny, nz); cudaDeviceSynchronize();
        }
        swap(P,P2);
        if ((it % nsave)==0){ 
            #ifdef RAND
        	SaveArray(P ,nx,ny,nz,"P");
            #else
            SaveArray(P ,nx,ny,nz,"Pb");
            #endif
         	isave = isave+1; 
        }
    }//it
    tim("Perf. FDTD wave 3D", Nix*Niy*Niz*(nt-3)*2*PRECIS/(1e9)); // timer test
    printf("Process %d used GPU with id %d.\n",me,gpu_id);
    #ifdef RAND
    isave=0; SaveArray(DV ,nx,ny,nz,"DV"); SaveArray(Src ,nt,1,1,"Src");
    #endif
    free_all(P);
    free_all(P2);
    free_all(dPx);
    free_all(dPy);
    free_all(dPz);
    // Random Field
    free_all(DV);
    free_all(__device_maxval);
    free_all(Src);
    // MPI

    clean_cuda();
    MPI_Finalize();
    return 0;
}

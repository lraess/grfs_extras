// Random Field generator 3D GPU Cuda aware MPI

#define PI 3.14159265358979323846
#define RND(rnd)  scaled  = ((double)rand() / (double)RAND_MAX); \
                  rnd     = (DAT)scaled;
#define RNDN(rnd) scaled  = ((double)rand() / (double)RAND_MAX); \
                  scaled2 = ((double)rand() / (double)RAND_MAX); \
                  scaled  = sqrt(-(double)2.0*log(scaled))*cos((double)2.0*PI*scaled2); \
                  rnd     = (DAT)scaled;
// Preprocessing
const DAT C = sf/sqrt(Ng);
// Computing physics kernels /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__global__ void compute1(int* coords, DAT V1, DAT V2, DAT V3, DAT a, DAT b, DAT* DV, DAT dx, DAT dy, DAT dz, const int nx,const int ny,const int nz){  
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z
    
    DAT x_g, y_g, z_g, tmp;

    if (iz<nz && iy<ny && ix<nx){ x_g = (DAT)(coords[0]*(nx-2) + ix+1)*dx - (DAT)0.5; }
    if (iz<nz && iy<ny && ix<nx){ y_g = (DAT)(coords[1]*(ny-2) + iy+1)*dy - (DAT)0.5; }
    if (iz<nz && iy<ny && ix<nx){ z_g = (DAT)(coords[2]*(nz-2) + iz+1)*dz - (DAT)0.5; }

    tmp = x_g*V1 + y_g*V2 + z_g*V3;

    if (iz<nz && iy<ny && ix<nx){ DV[ix + iy*nx + iz*nx*ny] = DV[ix + iy*nx + iz*nx*ny] + a*sin(tmp) + b*cos(tmp); }
}

__global__ void compute2(const DAT C, DAT* DV, const int nx,const int ny,const int nz){  
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

    if (iz<nz && iy<ny && ix<nx){ DV[ix + iy*nx + iz*nx*ny] = C*DV[ix + iy*nx + iz*nx*ny]; }
}

__global__ void plus_norm(DAT* DV, DAT max_val, const DAT scale, const int nx,const int ny,const int nz){
    int ix = blockIdx.x*blockDim.x + threadIdx.x; // thread ID, dimension x
    int iy = blockIdx.y*blockDim.y + threadIdx.y; // thread ID, dimension y
    int iz = blockIdx.z*blockDim.z + threadIdx.z; // thread ID, dimension z

    if (iz<nz && iy<ny && ix<nx){ DV[ix + iy*nx + iz*nx*ny] = (scale/max_val)*DV[ix + iy*nx + iz*nx*ny]; }
}
/////////////////////////////////////////
#define generate_random_field() \
        int flag, ig; \
        double scaled=0.0, scaled2=0.0; \
        srand(time(NULL)); \
        DAT fi=0.0, k=0.0, d=0.0, theta=0.0, rand1=0.0, a=0.0, b=0.0, V1=0.0, V2=0.0, V3=0.0; \
        tic(); \
        for (ig=0; ig < Ng; ig++){ \
            RND(rand1); \
            fi = (DAT)2.0*PI*rand1; \
            flag = 1; \
            while(flag==1){ \
                RND(rand1); \
                    k = tan(PI*(DAT)0.5*rand1); \
                    d = (k*k)/((DAT)1.0 + (k*k)); \
                    RND(rand1); \
                    if(rand1 < d){ flag = 0; } \
            } \
            RND(rand1); \
            theta = acos((DAT)1.0-(DAT)2.0*rand1); \
            V1    = k*sin(fi)*sin(theta) / If[0]; \
            V2    = k*cos(fi)*sin(theta) / If[1]; \
            V3    = k*cos(theta)         / If[2]; \
            RNDN(a); \
            RNDN(b); \
            compute1<<<grid, block>>>(coords_d, V1, V2, V3, a, b, DV_d, dx, dy, dz, nx, ny, nz);  cudaDeviceSynchronize(); \
        } \
        compute2<<<grid, block>>>(C, DV_d, nx, ny, nz);  cudaDeviceSynchronize(); \
        tim("Perf. Random Field", Nix*Niy*Niz*Ng*2*PRECIS/(1e9)); tic(); \
        __MPI_max(DV ,nx,ny,nz); \
        plus_norm<<<grid, block>>>(DV_d, DV_MAX, scale, nx, ny, nz);  cudaDeviceSynchronize(); \
        tim("Time normalisation", 0);

function A = load_field(var_name, nxyz_l_A, nxyz_l, OVERLAP, nprocs, coords, DAT, isave, skip, machineformat)

% A_l  = zeros(nxy_l_A,DAT);

for p=1:nprocs
    fname = [int2str(isave) '_' int2str(p-1) '_' var_name '.res'];
    % fname = [int2str(p-1) '_' var_name '.res'];
    fid   = fopen(fname,'rb'); count=inf; if (fid==-1), A=NaN; return; end
    A_l   = fread(fid, count, DAT, skip, machineformat);  fclose(fid);
    A_l   = reshape(A_l, nxyz_l_A);

    A_x  = [1:nxyz_l_A(1)] + coords(p,1)*(nxyz_l(1)-OVERLAP);
    A_y  = [1:nxyz_l_A(2)] + coords(p,2)*(nxyz_l(2)-OVERLAP);
    A_z  = [1:nxyz_l_A(3)] + coords(p,3)*(nxyz_l(3)-OVERLAP);

    A(A_x, A_y, A_z) = A_l;
end
